# Jonathan Bueno's CI&T Test

I have used:

- Vue.js;
- Vue-router;
- Vuex;
- Axios (fake API);
- Webpack;
- HTML/SASS/JS;
- Responsive Layout;
- Caching data for improve the performance.

## Install Dependencies


```bash
npm install
```

## Run Development Mode


```bash
npm run dev
```

## Run Unit Tests


```bash
npm run test
```