import Vue from 'vue';
import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

import Search from '../components/search/search.vue';

Vue.use(Vuex);

describe('Search Component:', () => {
    let store;
    let actions;
    let state;
    let mutations;

    beforeEach(() => {
        state = { filter: '' };

        actions = {
            filterProducts: jest.fn()
        };

        mutations = {
          'FILTER_PRODUCTS' (state, filter) {
            state.filter = filter
          }
        }

        store = new Vuex.Store({
          state,
          actions,
          mutations
        });
        
      });

      it('dispatches filterProducts action when Search Input has value;', () => {
        const wrapperStore = shallowMount(Search, {
          store
        });
        
        const input = wrapperStore.find('input');

        input.element.value = 'George';
        input.trigger('input');

        expect(actions.filterProducts).toHaveBeenCalled();
      })

      it('after the dispatch, it commits the FILTER_PRODUCTS mutation, that sets new value in State.filter', () => {
        mutations.FILTER_PRODUCTS(state, 'George')
        expect(state.filter).toEqual('George')
      })
})