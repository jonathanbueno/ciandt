import Vue from 'vue';
import Vuex from 'vuex';
import getters from '../store/productsList/getters.js'

Vue.use(Vuex);

describe('Products List Component:', () => {
    it("has value in computed property filteredItems(), with the state.filter's value", () => {
        const state = {
            products: '',
            filter: 'George'
        }

        expect(getters.filter(state)).toEqual('George')

    })
})
