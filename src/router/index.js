/* eslint-disable */

import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/home/home.vue'
import Page404 from '../components/404.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
        path: '/',
        name: 'Home',
        base: '',
        component: Home
    },
    { 
        path: "*",
        name: 'Página Não Encontrada',
        component: Page404 
    }
  ],
  mode: 'history'
})
