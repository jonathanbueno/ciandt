import Vue from 'vue';
import Vuex from 'vuex';
import productsList from './productsList/index.js'

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    productsList
  },
});