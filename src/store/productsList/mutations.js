export default {
    'LIST_PRODUCTS' (state, products) {
        state.products = products
    },
    'FILTER_PRODUCTS' (state, filter) {
        state.filter = filter
    }
}