/* eslint-disable */

import axios from 'axios'

export default {
    listProducts (context, products) {
        return new Promise((resolve, reject) => {
            axios.get('https://reqres.in/api/users?page=1&per_page=6')
            .then(response => {
                context.commit('LIST_PRODUCTS', response.data)
                resolve(response)
                localStorage.products = response.data.data //Improving the perfomance <3
            })
            .catch(error => {
                console.log('Error in listProducts action: '+error)
            });
        });
    },
    filterProducts (context, filter) {
        context.commit('FILTER_PRODUCTS', filter.search)
    }
}